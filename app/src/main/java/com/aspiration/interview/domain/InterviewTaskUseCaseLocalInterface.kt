package com.aspiration.interview.domain

import com.aspiration.interview.data.db.entities.Element
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response

interface InterviewTaskUseCaseLocalInterface {
    // TODO add your interface method for implementation here
    fun fetchData(): Single<List<Element>>

}