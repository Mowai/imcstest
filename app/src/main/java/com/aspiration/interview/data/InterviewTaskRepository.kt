package com.aspiration.interview.data

import com.aspiration.interview.data.db.daos.ElementDao
import com.aspiration.interview.data.db.entities.Element
import com.aspiration.interview.data.network.service.ApiService
import com.aspiration.interview.domain.repository.InterviewTaskRepositoryInterface
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response


class InterviewTaskRepository(private val api: ApiService, private val elementDao:ElementDao): InterviewTaskRepositoryInterface {
    // TODO fetching methods here. It should contain fetching from API and mapping to presentation/domain models

    override fun fetchData(): Observable<Response<List<Element>>> = api.fetchData()

    override fun fetchLocalData(): Single<List<Element>> = elementDao.getElements()

    override fun saveData(list: List<Element>) = elementDao.insertAll(list)


}