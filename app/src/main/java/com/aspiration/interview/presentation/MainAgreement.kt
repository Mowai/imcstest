package com.aspiration.interview.presentation

import com.aspiration.interview.data.db.entities.Element
import com.aspiration.interview.presentation.base.BasePresenter
import com.aspiration.interview.presentation.base.BaseView

interface MainAgreement {
    interface View : BaseView {
        // TODO set UI reflection methods
        fun showList(list:List<Element>)
        fun showError(message:String)

    }

    interface Presenter : BasePresenter<View> {
        // TODO set API request (usecase) methods invocation
        fun fetchData()
        fun fetchLocalData()
    }
}