package com.aspiration.interview.data.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "elements")
data class Element(

    @PrimaryKey
    @SerializedName("id")
    @ColumnInfo(name = "id") var id: Int,

    @ColumnInfo(name = "title")
    @SerializedName("title") var title: String,

    @ColumnInfo(name = "body")
    @SerializedName("body") var body: String,

    @ColumnInfo(name = "header") var header: String

)
