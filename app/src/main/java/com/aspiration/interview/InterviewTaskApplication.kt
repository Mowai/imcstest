package com.aspiration.interview

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.widget.Toast
import com.aspiration.interview.di.ApplicationComponent
import com.aspiration.interview.di.DaggerApplicationComponent
import com.aspiration.interview.di.modules.ApplicationModule
import com.aspiration.interview.di.modules.NetworkingModule
import com.aspiration.interview.di.modules.RoomModule

class InterviewTaskApplication: Application() {
    override fun onCreate() {
        super.onCreate()

        applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .networkingModule(NetworkingModule("https://jsonplaceholder.typicode.com/"))
            .roomModule(RoomModule(this))
            .build()

        isInternetAvailable(this)

    }


    private fun isInternetAvailable(context: Context) {
        var result = false
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = connectivityManager.activeNetwork
            if(networkCapabilities != null){
                val actNw = connectivityManager.getNetworkCapabilities(networkCapabilities)
                actNw?.let {
                    result = when {
                        actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                        actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                        actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                        else -> false
                    }
                }

            }
        } else {
            connectivityManager.run {
                connectivityManager.activeNetworkInfo?.run {
                    result = when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }

                }
            }
        }

        if (!result) Toast.makeText(this,"No network connection", Toast.LENGTH_SHORT ).show()
    }


    companion object {
        var applicationComponent: ApplicationComponent? = null
    }
}