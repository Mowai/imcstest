package com.aspiration.interview.presentation

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.aspiration.interview.R
import com.aspiration.interview.data.db.entities.Element

class CustomAdapter(private val list : MutableList<Element>) : RecyclerView.Adapter <CustomAdapter.CustomHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomAdapter.CustomHolder {
        val inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.item_row, parent, false)
        return CustomHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: CustomAdapter.CustomHolder, position: Int) {
        val item = list[position]
        holder.bindElement(item)
    }

    override fun getItemCount(): Int = list.size

    @SuppressLint("NotifyDataSetChanged")
    fun update(updatedList: List<Element>) {
        list.clear()
        list.addAll(updatedList)
        notifyDataSetChanged()
    }

    class CustomHolder(private val v: View) : RecyclerView.ViewHolder(v) {

        fun bindElement (item : Element){
            val header = v.findViewById<TextView>(R.id.header)
            val title = v.findViewById<TextView>(R.id.title)
            val body = v.findViewById<TextView>(R.id.body)
            header.text = item.header
            title.text = item.title
            body.text = item.body
        }
    }


}