package com.aspiration.interview.domain.repository

import com.aspiration.interview.data.db.entities.Element
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response

interface InterviewTaskRepositoryInterface {
    // TODO Add your method for getting data here
    fun fetchData() : Observable<Response<List<Element>>>

    fun fetchLocalData() : Single<List<Element>>

    fun saveData(list : List<Element>)
}