package com.aspiration.interview.data.network.service


import com.aspiration.interview.data.db.entities.Element
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET


interface ApiService {
    // TODO add your endpoint here
    @GET("https://jsonplaceholder.typicode.com/posts")
    fun fetchData(): Observable<Response<List<Element>>>
}