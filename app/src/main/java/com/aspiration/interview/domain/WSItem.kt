package com.aspiration.interview.domain

import com.google.gson.annotations.SerializedName
import java.util.*

class WSItem {
    @SerializedName("userId")
    var userId: Int? = null
    @SerializedName("id")
    var id: Int? = null
    @SerializedName("title")
    var title: String? = null
    @SerializedName("body")
    var body: String? = null

    //adding header
    var header:String? = null
}