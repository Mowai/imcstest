package com.aspiration.interview.domain

import android.util.Log
import com.aspiration.interview.data.db.entities.Element
import com.aspiration.interview.domain.repository.InterviewTaskRepositoryInterface
import io.reactivex.Scheduler
import io.reactivex.Single

class InterviewTaskUseCaseLocal(
    private val ioScheduler: Scheduler,
    private val repository: InterviewTaskRepositoryInterface
) :
    InterviewTaskUseCaseLocalInterface {
    override fun fetchData(): Single<List<Element>> = repository.fetchLocalData()
        .doOnSuccess{ Log.i("SERVICE", "Success")}
        .doOnError{
            Log.e("SERVICE", "Error")}
        .subscribeOn(ioScheduler)


}