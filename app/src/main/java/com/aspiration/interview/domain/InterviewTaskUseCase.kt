package com.aspiration.interview.domain

import android.util.Log
import com.aspiration.interview.data.db.entities.Element
import com.aspiration.interview.domain.repository.InterviewTaskRepositoryInterface
import com.aspiration.interview.utils.toCustomDate
import io.reactivex.Observable
import io.reactivex.Scheduler
import org.json.JSONObject
import retrofit2.Response

class InterviewTaskUseCase(
    private val ioScheduler: Scheduler,
    private val repository: InterviewTaskRepositoryInterface
) :
    InterviewTaskUseCaseInterface {
    override fun fetchData(): Observable<Response<List<Element>>> = repository.fetchData()
        .map {
            if (it.isSuccessful){
                Log.i("SERVICE", "Success")
                it.body()?.forEach { item -> item.header = item.id.toCustomDate()}
                var list = it.body()
                if (list != null) {
                    repository.saveData(list)
                }
            }
            else {
                Log.e("SERVICE", "Error")
            }

            //it.body()
            it
        }
        .doOnEach { Log.i("SERVICE", "Success") }
        .doOnError{
            Log.e("SERVICE", "Error")}
        .subscribeOn(ioScheduler)



    // TODO mandatory: use usecase as a proxy for your data and just return what came from server
    // TODO mandatory: write logs that represents success/error state
    // TODO optional: add header after each item. For more details, look into Readme file
}