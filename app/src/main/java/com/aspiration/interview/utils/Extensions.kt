package com.aspiration.interview.utils

import android.view.View
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.concurrent.TimeUnit

fun Disposable.disposeBy(disposeBag: CompositeDisposable) =
    this.also { disposeBag.add(it) }

fun <T : Any> Observable<T>.throttleFirst(): Observable<T> = throttleFirst(THROTTLE_TIME, TimeUnit.MILLISECONDS)

const val THROTTLE_TIME = 500L

fun View.hide() {
    this.visibility = View.GONE
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun Int.toCustomDate() : String {
    var cal = Calendar.getInstance()
    cal.time = Date()
    cal.add(Calendar.HOUR,this)

    val dateFormat = SimpleDateFormat("EEE MMM yy HH:SS:SSS")
    return dateFormat.format(cal.time)
}

//val datePlusOneMonth = Calendar.getInstance().run {
//    add(Calendar.MONTH, 1)
//    time
//}