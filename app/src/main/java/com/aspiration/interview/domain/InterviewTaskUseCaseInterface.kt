package com.aspiration.interview.domain

import com.aspiration.interview.data.db.entities.Element
import io.reactivex.Observable
import retrofit2.Response

interface InterviewTaskUseCaseInterface {
    // TODO add your interface method for implementation here
    fun fetchData(): Observable<Response<List<Element>>>
}