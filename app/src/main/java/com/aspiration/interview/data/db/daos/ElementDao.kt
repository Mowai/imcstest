package com.aspiration.interview.data.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aspiration.interview.data.db.entities.Element
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface ElementDao {

    /**
     * Get all elements.

     * @return element list.
     */
    @Query("SELECT * FROM elements" )
    fun getElements(): Single<List<Element>>
    /**
     * Get a element by id.

     * @return the element from the table with a specific id.
     */
    @Query("SELECT * FROM Elements WHERE id = :id")
    fun getElementById(id: Int): Element

    /**
     * Insert a element in the database. If the element already exists, replace it.

     * @param element the element to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertElement(element: Element): Completable

    /**
     * Delete all elements.
     */
    @Query("DELETE FROM elements")
    fun deleteAllElements()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(elements: List<Element>)
}