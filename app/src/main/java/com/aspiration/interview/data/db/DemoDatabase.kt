package com.aspiration.interview.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.aspiration.interview.data.db.daos.ElementDao
import com.aspiration.interview.data.db.entities.Element

@Database(entities = [Element::class], version = 1, exportSchema = false)
abstract class DemoDatabase : RoomDatabase() {

    abstract fun elementDao(): ElementDao

    /*companion object {

        @Volatile private var INSTANCE: DemoDatabase? = null

        fun getInstance(context: Context): DemoDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                DemoDatabase::class.java, "Sample.db")
                .build()
    }*/
}
