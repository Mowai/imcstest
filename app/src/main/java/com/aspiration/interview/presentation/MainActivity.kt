package com.aspiration.interview.presentation

import android.widget.Button
import android.widget.ProgressBar
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aspiration.interview.InterviewTaskApplication
import com.aspiration.interview.R
import com.aspiration.interview.data.db.entities.Element
import com.aspiration.interview.presentation.base.BaseActivity
import com.aspiration.interview.utils.hide
import com.aspiration.interview.utils.show


class MainActivity :
    BaseActivity<MainAgreement.Presenter, MainAgreement.View>(R.layout.activity_main),
    MainAgreement.View {

    private lateinit var fetchBtn: Button
    private lateinit var progress: ProgressBar
    private lateinit var recycler: RecyclerView
    private lateinit var adapter: CustomAdapter

    override fun onResume() {
        super.onResume()
        presenter.fetchLocalData()
    }

    override fun setupInputs() {
        InterviewTaskApplication.applicationComponent?.inject(this)
        fetchBtn = findViewById(R.id.btnFetch)
        progress = findViewById(R.id.progressbar)
        recycler = findViewById(R.id.item_list)
        adapter = CustomAdapter(mutableListOf())
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = adapter

        // TODO add your click listeners here

        val subscribeButton =  fetchBtn.bindClick { presenter.fetchData()}
        compositeDisposable.addAll(subscribeButton)

    }





    override fun showLoading() {
        progress.show()
        // TODO show progress when data is loading
    }

    override fun hideLoading() {
        progress.hide()
    }

    // TODO add method for data population to the recycler view
    // TODO add method for showing error state

    override fun showList(list: List<Element>) {
        adapter.update(list)
    }

    override fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        presenter.fetchLocalData()
    }



    override fun returnThisHerePlease(): MainAgreement.View = this

}