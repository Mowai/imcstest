package com.aspiration.interview.presentation

import com.aspiration.interview.domain.InterviewTaskUseCaseInterface
import com.aspiration.interview.domain.InterviewTaskUseCaseLocalInterface
import com.aspiration.interview.presentation.base.BasePresenterImpl
import javax.inject.Inject

class MainPresenter @Inject constructor(private val useCase: InterviewTaskUseCaseInterface, private val useCaseLocal: InterviewTaskUseCaseLocalInterface) :
    BasePresenterImpl<MainAgreement.View>(), MainAgreement.Presenter {
    // TODO implement your methods for fetching here
    override fun fetchData() {
       val subscribe = useCase.fetchData().bindLoading().subscribe(
           {
               if (it.isSuccessful) {
                   var list = it?.body()
                   list?.let { view?.showList(list) }
               }else{
                   var message : String? = it?.message()
                   message?.let { it1 -> view?.showError("Error ${it.code()}: $message") }.run { view?.showError("Error ${it.code()}") }
               }

           },{
               it?.message?.let { view?.showError(it) }
           }
       )

        compositeDisposable.addAll(subscribe)
    }

    override fun fetchLocalData(){

        val subscribe = useCaseLocal.fetchData().bindLoading().subscribe(
            {
                  view?.showList(it)
            },{
                it?.message?.let { view?.showError(it) }
            }
        )

        compositeDisposable.addAll(subscribe)
    }

}