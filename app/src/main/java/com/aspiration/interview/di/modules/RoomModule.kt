package com.aspiration.interview.di.modules

import android.content.Context
import androidx.room.Room
import com.aspiration.interview.data.db.DemoDatabase
import com.aspiration.interview.data.db.daos.ElementDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class RoomModule(context: Context) {
    private val demoDatabase: DemoDatabase

    @Singleton
    @Provides
    fun providesRoomDatabase(): DemoDatabase {
        return demoDatabase
    }

    @Singleton
    @Provides
    fun providesElementDao(demoDatabase: DemoDatabase): ElementDao {
        return demoDatabase.elementDao()
    }



    init {
        demoDatabase =
            Room.databaseBuilder(context, DemoDatabase::class.java, "Sample.db").build()
    }
}